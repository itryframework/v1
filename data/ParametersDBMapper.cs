using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using it.itryframework.interfaces;
using it.itryframework.attributes;
using it.itryframework.managers.config;
using it.itryframework.managers.connection;
using it.itryframework.config;
using System.Text.RegularExpressions;
using it.itryframework.managers.log;

namespace it.itryframework.data
{
    public sealed class ParametersDBMapper
    {
        private static string PREFISSO_PKEY = "key_";

        static DALConfigurationSection _DALConfig;
        static LogConfigurationSection _logConfig;

        #region costruttore

        static ParametersDBMapper()
        {            
            _DALConfig = ConfigManager.getDALConfigValues();
            _logConfig = ConfigManager.getLogConfigValues();
        }

        #endregion

        #region private methods
        #region _select
        private static bool _select(IGenericObject obj, DBColumns parameters)
        {
            StringBuilder sql = new StringBuilder();
            string where = string.Empty;
            sql.Append(string.Format("SELECT * FROM {0} WHERE 1 = 1 ", obj.TableName));
            DBColumns validParameters = null;

            if (parameters != null && parameters.Count > 0)
            {
                validParameters = parameters;
            }
            else
            {
                validParameters = new DBColumns();

                //prendo dall'oggetto obj il valore della chiave primaria; 
                Dictionary<string, object> dic = null;
                try { dic = _getDic(obj, false, 3); }   //dic contiene chiave>>nome colonna | valore>>valore campo
                catch { throw; }
                foreach (KeyValuePair<string, object> keyVal in dic)
                {
                    if (keyVal.Key.Contains(PREFISSO_PKEY))
                    {
                        validParameters.Add(keyVal.Key.Substring(PREFISSO_PKEY.Length), keyVal.Value);
                        break;
                    }
                }
            }

            foreach (DBColumn parameter in validParameters)
            {
                if (DBColumn.IsValid(parameter))
                {
                    sql.Append(string.Format(" AND {0} = @_{0}", parameter.Field));
                }
            }

            IDataReader reader = null;
            bool hasRows = false;
            try
            {
                using (reader = getIDataReader(sql.ToString(), validParameters))
                {
                    while (reader.Read())
                    {
                        setObject(obj, reader);
                        hasRows = true;
                    }
                }
                return hasRows;
            }
            catch (Exception ex) { _writeLog(ex, sql.ToString()); throw; }
            finally { if (reader != null) reader.Close(); }
        }
        #endregion

        #region _readerContains
        private static bool _readerContains(IDataReader reader, string dbAttrName)
        {
            DataView view = reader.GetSchemaTable().DefaultView;
            view.RowFilter = "ColumnName='" + dbAttrName + "'";
            return view.Count > 0;
        }
        #endregion

        #region _getDic
        /// <summary>
        /// Ritorna una dictionary con chiave pari al nome della colonna del db e value il valore della propriet� convertito a stringa.
        /// </summary>
        /// <param name="obj">IGenericObject</param>
        /// <param name="checkIfIgnorePKey"></param>
        /// <param name="tipoOp">0 tutte, 1 insert, 2 update, 3 select, 4 delete</param>
        /// <returns>System.Collections.Generic.Dictionary</returns>
        /// <exception cref="System.Exception">System.Exception</exception>
        private static Dictionary<string, object> _getDic(IGenericObject obj, bool checkIfIgnorePKey, int tipoOp)
        {
            StringBuilder sbColumns = new StringBuilder();
            StringBuilder sbValues = new StringBuilder();
            Dictionary<string, object> dic = new Dictionary<string, object>();
            Type _type = obj.GetType();
            PropertyInfo[] props = _type.GetProperties();

            foreach (PropertyInfo pInfo in props)
            {
                object[] attributes = pInfo.GetCustomAttributes(typeof(DBAttributes), true);
                if (attributes.Length == 0) continue;
                bool isKey = false;
                foreach (object attr in attributes)
                {
                    DBAttributes dbAttr = (DBAttributes)attr;
                    string dbColName = dbAttr.DbColumnName; //nome reale della colonna nel db

                    if (dbColName.Equals(obj.PrimaryKey, StringComparison.InvariantCultureIgnoreCase))
                    {
                        dbColName = (checkIfIgnorePKey && dbAttr.IgnoreOnInsert ? "" : PREFISSO_PKEY) + pInfo.Name;
                        isKey = true;
                        //dic.Add(PREFISSO_PKEY + pInfo.Name, Convert.ToString(pInfo.GetValue(obj, null)));
                        //una volta registrata la chiave passa alla prossima propriet�
                        //continue;
                    }
                    
                    if (dbAttr.ReadOnly)
                    {
                        switch (tipoOp)
                        {
                            case 1: continue; // 1 insert
                            case 2: if (!isKey) continue; break; // 2 update
                            case 3: break; // 3 select
                            case 4: break; // 4 delete
                        }
                    }

                    try
                    {
                        dic.Add(dbColName, pInfo.GetValue(obj, null));
                    }
                    catch
                    {
                        throw;
                    }
                }
            }

            return dic;
        }
        #endregion

        #region _executeSql
        /// <summary>
        /// Esegue la query passata.
        /// </summary>
        /// <param name="sql">query da eseguire</param>
        /// <param name="caller">il chiamante la funzione</param>
        /// <returns>System.Int32</returns>
        /// <exception cref="System.InvalidOperationException">System.InvalidOperationException</exception>
        private static int _executeSql(string sql, DBColumns parameters, string caller)
        {
            // Sostituisco tutti gli @parametro con @_parametro per evitare keywords
            string replacedSql = Regex.Replace(sql, "@([^_]{1})", "@_$1");

            _setLogSql(caller, replacedSql);

            IDbConnection cn = null;
            try { cn = ConnectionManager.Open(); }
            catch (Exception ex)
            {
                if (cn != null) cn.Close();
                _writeLog(ex, replacedSql);
                throw;
            }
            IDbCommand cmd = cn.CreateCommand();
            cmd.CommandText = replacedSql;
            cmd.CommandType = CommandType.Text;
            if (parameters != null)
            {
                parameters.AddToIDbCommand(cmd);
            }

            int result = 0;
            try
            {
                result = cmd.ExecuteNonQuery();
                cn.Close();
                return result;
            }
            catch (Exception ex2)
            {
                if (cn != null) cn.Close();
                _writeLog(ex2, replacedSql);

                throw;
            }
        }
        #endregion

        #region _setLogSql
        private static void _setLogSql(string caller, string sql)
        {
            if (_logConfig != null)
            {
                if (_logConfig.SqlToVsOutputWindow)
                {
                    System.Diagnostics.Debug.WriteLine((!string.IsNullOrEmpty(caller) ? caller : "") + ", executing sql:" + sql);
                }
            }
        }
        #endregion

        #region _fillTable
        private static DataTable _fillTable(string sql, DBColumns parameters, string caller)
        {
            string replacedSql = Regex.Replace(sql, "@([^_]{1})", "@_$1");
            _setLogSql(caller, replacedSql);

            IDbConnection cn = null;
            try { cn = ConnectionManager.Open(); }
            catch (Exception ex)
            {
                if (cn != null) cn.Close();
                _writeLog(ex, replacedSql);
                throw;
            }

            IDbDataAdapter da = null;
            DataSet ds = new DataSet();

            string assemblyName = cn.GetType().Assembly.FullName;
            string typeName = cn.GetType().FullName.Replace("Connection", "DataAdapter");
            try
            {
                da = (IDbDataAdapter)Activator.CreateInstance(assemblyName, typeName).Unwrap();
            }
            catch (Exception ex1)
            {
                if (cn != null) cn.Close();
                _writeLog(ex1, replacedSql);
                throw;
            }

            IDbCommand cmd = cn.CreateCommand();
            cmd.CommandText = replacedSql;
            if (parameters != null)
                parameters.AddToIDbCommand(cmd);

            da.SelectCommand = cmd;

            try
            {
                da.Fill(ds);
                cn.Close();
                return (ds.Tables.Count > 0 && ds.Tables[0] != null) ? ds.Tables[0] : null;
            }
            catch { cn.Close(); throw; }
        }
        #endregion

        #region getStringWithQuery, getIntWithQuery, getBoolWithQuery
        /// <summary>
        /// Indicato per query di SELECT che restituiscono un solo valore.
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static string getStringWithQuery(string sql, DBColumns parameters)
        {
            return _getValueWithQuery<string>(sql, parameters);
        }
        /// <summary>
        /// Indicato per query di SELECT che restituiscono un solo valore.
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static bool getBoolWithQuery(string sql, DBColumns parameters)
        {
            return _getValueWithQuery<bool>(sql, parameters);
        }
        /// <summary>
        /// Indicato per query di SELECT che restituiscono un solo valore.
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>

        public static int getIntWithQuery(string sql, DBColumns parameters)
        {
            return _getValueWithQuery<int>(sql, parameters);
        }

        private static T _getValueWithQuery<T>(string sql, DBColumns parameters)
        {
            if (string.IsNullOrEmpty(sql)) return default(T);

            object val = executeScalar(sql, parameters, false);
            if (val is DBNull) return default(T);
            if (val == null) return default(T);
            return (T)Convert.ChangeType(val, typeof(T));
        }

        #endregion

        #region _writeLog

        private static void _writeLog(Exception ex, string customMessage)
        {
            _writeLog(ex, new string[] { customMessage });
        }

        private static void _writeLog(Exception ex, string[] messages)
        {
            StringBuilder s = new StringBuilder();
            foreach (string token in messages)
            {
                s.Append(token + "<br />");
            }
            LogManager.trace(ex, s.ToString());
        }

        #endregion

        #region _getToken4LastInsertId

        private static string _getToken4LastInsertId()
        {
            if (_DALConfig == null)
                return "";
            
            if (_DALConfig.TipoDb == "sql_server")
                return "; SELECT SCOPE_IDENTITY()";

            if (_DALConfig.TipoDb == "mysql")
                return "; SELECT LAST_INSERT_ID()";

            return "";
        }
        #endregion

        #region _getChangedType
        public static object _getChangedType(object value, Type conversion)
        {
            var t = conversion;

            if (t.IsGenericType && t.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                if (value == null)
                {
                    return null;
                }

                t = Nullable.GetUnderlyingType(t);
            }

            return Convert.ChangeType(value, t);
        }
        #endregion

        #endregion

        #region public methods

        #region setObject
        /// <summary>
        /// Setta l'oggetto IObj passato con i valori contenuti nel reader passato.
        /// </summary>
        /// <param name="IObj">oggetto che implementa l'interfaccia IGenericObject e gli attributi DbAttributes</param>
        /// <param name="reader">reader valido</param>
        /// <exception cref="System.InvalidCastException">System.InvalidCastException</exception>
        public static void setObject(IGenericObject IObj, IDataReader reader)
        {
            PropertyInfo[] pInfos = IObj.GetType().GetProperties();
            foreach (PropertyInfo pInfo in pInfos)
            {
                object[] arr = pInfo.GetCustomAttributes(typeof(DBAttributes), true);
                if (arr.Length == 0)
                    continue;

                DBAttributes _attr = (DBAttributes)arr[0];   //ce n'� e ce ne deve essere uno solo
                if (!_readerContains(reader, _attr.DbColumnName))
                    continue;

                object val = reader[_attr.DbColumnName];
                if (val is DBNull)
                {
                    val = null;
                }
                else
                {
                    try
                    {
                        val = _getChangedType(val, pInfo.PropertyType);
                        //Convert.ChangeType(val, pInfo.PropertyType); 
                    }
                    catch { throw; }
                }
                pInfo.SetValue(IObj, val, null);
            }
        }
        #endregion

        #region insert
        /// <summary>
        /// Inserisce una riga nel database e ritorna l'id inserito (solo per database SQL SERVER, -1 per altri tipi di database).
        /// </summary>
        /// <param name="obj">IGenericObject</param>
        /// <returns>System.Int32</returns>
        /// <exception cref="System.Exception">System.Exception</exception>
        public static int insert(IGenericObject obj)
        {
            StringBuilder sql = new StringBuilder("INSERT INTO " + obj.TableName + "(");
            StringBuilder sbColumns = new StringBuilder();
            StringBuilder sbValues = new StringBuilder();
            DBColumns parameters = new DBColumns();
            Dictionary<string, object> dic = null;
            try { dic = _getDic(obj, true, 1); }   //dic contiene chiave>>nome colonna | valore>>valore campo
            catch { throw; }

            bool hasPKey = false;   //se diventa true allora posso prendere l'ultimo id inserito quando eseguo la query
            foreach (KeyValuePair<string, object> keyVal in dic)
            {
                if (keyVal.Key.Contains(PREFISSO_PKEY))
                {
                    hasPKey = true;
                    continue;    //salto la chiave che ha prefisso key_
                }

                if (sbColumns.Length > 0) sbColumns.Append(",");
                if (sbValues.Length > 0) sbValues.Append(",");

                parameters.Add(keyVal.Key, keyVal.Value);
                sbColumns.Append(keyVal.Key);
                sbValues.Append("@_" + keyVal.Key);
            }

            sql.Append(sbColumns.ToString() + ") VALUES (" + sbValues.ToString() + ")");

            int result = -1;
            try
            {
                result = Convert.ToInt32(executeScalar(sql.ToString(), parameters, hasPKey));
                return result;
            }
            catch (Exception ex)
            {
                _writeLog(ex, sql.ToString());
                throw;
            }
        }

        #endregion

        #region update
        /// <summary>
        /// Aggiorna il database con l'oggetto passato.
        /// Ritorna True se vi sono state modifiche, False in caso contrario
        /// </summary>
        /// <param name="obj">IGenericObject</param>
        /// <returns>System.Boolean</returns>
        /// <exception cref="System.InvalidOperationException">System.InvalidOperationException</exception> 
        /// <exception cref="System.Exception">System.Exception</exception>
        public static bool update(IGenericObject obj) { return update(obj, null); }

        /// <summary>
        /// Aggiorna il database con l'oggetto passato e con le clausole where indicate in System.Collections.Generic.Dictionary (nomeColonna,valoreColonna).
        /// Ritorna True se vi sono state modifiche, False in caso contrario.
        /// </summary>
        /// <param name="obj">IGenericObject</param>
        /// <param name="lstWhere">System.Collections.Generic.Dictionary in chiave una stringa contenente il valore della colonna, in value 
        /// solo int o string, per ogni altro valore lancia un'eccezione.</param>
        /// <returns>System.Boolean</returns>
        /// <exception cref="System.Exception">System.Exception</exception>
        public static bool update(IGenericObject obj, DBColumns lstWhere)
        {
            string pKeyVal = string.Empty;
            string pKeyName = string.Empty;
            StringBuilder sql = new StringBuilder("UPDATE " + obj.TableName + " SET ");
            StringBuilder sqlPart = new StringBuilder();
            StringBuilder sbWhere = new StringBuilder();
            StringBuilder sbWhereKeys = new StringBuilder();

            DBColumns parameters = new DBColumns();

            Dictionary<string, object> dic = null;
            try { dic = _getDic(obj, false, 2); }   //dic contiene chiave>>nome colonna | valore>>valore campo
            catch { throw; }

            foreach (KeyValuePair<string, object> keyVal in dic)
            {
                //salta il campo che corrisponde alla propriet� dell'interfaccia IGenericObject PrimaryKey
                if (keyVal.Key.Contains(PREFISSO_PKEY))
                {
                    //tolgo il prefisso
                    pKeyName = keyVal.Key.Substring(PREFISSO_PKEY.Length);
                    parameters.Add(pKeyName, keyVal.Value);
                    if (sbWhereKeys.Length > 0)
                        sbWhereKeys.Append(" AND ");
                    sbWhereKeys.Append(string.Format("{0} = @_{0}", pKeyName));
                }
                else
                {
                    if (sqlPart.Length > 0)
                        sqlPart.Append(",");

                    sqlPart.Append(string.Format("{0} = @_{0}", keyVal.Key));
                    parameters.Add(keyVal.Key, keyVal.Value);
                }
            }

            if (lstWhere == null)
            {
                // Se non sono state passate clausole WHERE, utilizzo la chiave primaria
                sbWhere = sbWhereKeys;
            }
            else
            {
                // clausole where
                bool validWhere = false;
                foreach (DBColumn cWhere in lstWhere)
                {
                    if (!DBColumn.IsValid(cWhere))
                        continue;

                    validWhere = true;

                    if (sbWhere.Length > 0)
                        sbWhere.Append(" AND ");

                    sbWhere.Append(string.Format("{0} = @_{0}", cWhere.Field));
                    parameters.Add(cWhere);
                }

                if (!validWhere)
                    sbWhere = sbWhereKeys;
            }

            if (sbWhere.Length <= 0)
                throw new Exception("Invalid WHERE clause");

            sql.Append(sqlPart.ToString() + " WHERE (" + sbWhere.ToString() + ")");
            
            int result = 0;
            try
            {
                result = _executeSql(sql.ToString(), parameters, "update");
                return (result == 1) ? true : false;
            }
            catch (Exception ex) { _writeLog(ex, sql.ToString()); throw; }
        }
        #endregion

        #region delete
        /// <summary>
        /// Cancella una riga dal database.
        /// Ritorna True in caso di modifiche, False in caso contrario.
        /// </summary>
        /// <param name="obj">IGenericObject</param>
        /// <returns>System.Boolean</returns>
        /// <exception cref="System.Exception">System.Exception</exception>
        public static bool delete(IGenericObject obj)
        {
            StringBuilder sql = new StringBuilder("DELETE FROM " + obj.TableName);
            string pKeyValue = string.Empty;
            DBColumns parameters = new DBColumns();

            Dictionary<string, object> dic = null;
            try { dic = _getDic(obj, false, 4); }   //dic contiene chiave>>nome colonna | valore>>valore campo
            catch { throw; }

            sql.Append(" WHERE ");

            foreach (KeyValuePair<string, object> keyVal in dic)
            {
                if (!keyVal.Key.Contains(PREFISSO_PKEY))
                    continue;

                string dbKeyName = keyVal.Key.Substring(PREFISSO_PKEY.Length);
                sql.Append(string.Format("{0} = @_{0}", dbKeyName));
                parameters.Add(dbKeyName, keyVal.Value);
            }

            int result = 0;
            try
            {
                result = _executeSql(sql.ToString(), parameters, "delete");
                return (result == 1) ? true : false;
            }
            catch (Exception ex) { _writeLog(ex, sql.ToString()); throw; }
        }
        #endregion

        #region select
        /// <summary>
        /// Carica un oggetto che implementa l'interfaccia IGenericObject ed espone attributi di tipo DBAttributes.
        /// Nella composizione del comando sql da eseguire PKeyValue verr� trattato come stringa.
        /// Ritorna TRUE nel caso in cui sia stato riempito l'oggetto, FALSE in caso contrario.
        /// </summary>
        /// <param name="obj">IGenericObject.</param>
        /// <returns>TRUE se caricato, FALSE in caso contrario.</returns>
        /// <exception cref="System.InvalidCastException">System.InvalidCastException quando si settano le propriet�</exception>
        /// <exception cref="System.Exception">System.Exception</exception>
        public static bool select(IGenericObject obj) { return _select(obj, null); }
        /// <summary>
        /// Carica un oggetto che implementa l'interfaccia IGenericObject ed espone attributi di tipo DBAttributes.
        /// La ricerca viene filtrata per il campo nomeCampoID passato.
        /// Nella composizione del comando sql da eseguire se il campo PKeyValue � di tipo string verr� trattato come stringa
        /// altrimenti verr� trattato come intero.
        /// Ritorna TRUE nel caso in cui sia stato riempito l'oggetto, FALSE in caso contrario.
        /// </summary>
        /// <param name="obj">IGenericObject.</param>
        /// <param name="nomeCampoId">nome del campo chiave.</param>
        /// <param name="PKeyValue">valore campo chiave.</param>
        /// <returns>TRUE se caricato, FALSE in caso contrario.</returns>
        /// <exception cref="System.InvalidCastException">System.InvalidCastException quando si settano le propriet�</exception>
        /// <exception cref="System.Exception">System.Exception</exception>
        public static bool select(IGenericObject obj, string nomeCampoId, object PKeyValue) { return _select(obj, new DBColumns() { new DBColumn(nomeCampoId, PKeyValue) }); }
        /// <summary>
        /// Restituisce un array di oggetti che implementano l'interfaccia IGenericObject.
        /// </summary>
        /// <typeparam name="T">oggetto che implementa l'interfaccia IGenericObject</typeparam>
        /// <param name="sql">query da eseguire</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">System.Exception</exception>
        public static T[] select<T>(string sql, DBColumns parameters) where T : IGenericObject
        {
            List<T> list = new List<T>();
            IDataReader reader = null;
            try
            {
                reader = getIDataReader(sql, parameters);
                while (reader.Read())
                {
                    T newT = Activator.CreateInstance<T>();
                    setObject(newT, reader);
                    list.Add(newT);
                }
                return (list.Count > 0) ? list.ToArray() : null;
            }
            catch (Exception ex) {
                _writeLog(ex, sql); throw;
            }
            finally { if (reader != null) reader.Close(); }
        }

        public static T[] selectRaw<T>(string sql) where T : IGenericObject
        {
            return select<T>(sql, null);
        }

        /// <summary>
        /// Ritorna un DataTable riempito in base alla stringa sql passata.
        /// </summary>
        /// <param name="sql">string sql da eseguire.</param>
        /// <returns>System.Data.DataTable</returns>
        /// <exception cref="System.Exception">System.Exception</exception>
        public static DataTable select(string sql, DBColumns parameters)
        {
            DataTable dt = null;
            try { dt = _fillTable(sql, parameters, "(get DataTable) select"); return dt; }
            catch (Exception ex) { _writeLog(ex, sql); throw; }
        }

        public static DataTable selectRaw(string sql)
        {
            return select(sql, null);
        }

        /// <summary>
        /// Ritorna un DataTable. 
        /// Lancia un'eccezione se non � possibile inizializzare una connessione al database sottostante.
        /// Lancia un'eccezione in caso di errore nell'interrogazione al database.  
        /// </summary>
        /// <param name="obj">IGenericObject</param>
        /// <param name="orderBy">clausola order by o vuoto/nullo per nessun ordine</param>
        /// <returns>DataTable</returns>
        public static DataTable select(IGenericObject obj, string orderBy)
        {
            return select(obj, null, null, orderBy);
        }

        public static DataTable select(IGenericObject obj, DBColumns dic, bool? useSearchAnd, string orderBy)
        {
            return select(obj, dic, useSearchAnd, true, orderBy);
        }

        /// <summary>
        /// Ritorna un DataTable. 
        /// </summary>
        /// <param name="obj">IGenericObject</param>
        /// <param name="dic">Dictionary contenente le clausole where da applicare. La forma � chiave == nomecolonna; valore == valore colonna.</param>
        /// <param name="useSearchAnd">TRUE per legare le clausole WHERE in AND, FALSE per legarle in OR.</param>
        /// <param name="useLike">True per usare l'operatore Like quando il value � stringa</param>
        /// <param name="orderBy">clausola order by o vuoto/nullo per nessun ordine</param>
        /// <returns>System.Data.DataTable</returns>
        /// <exception cref="System.Exception">System.Exception</exception>
        public static DataTable select(IGenericObject obj, DBColumns dic, bool? useSearchAnd, bool useLike, string orderBy)
        {
            StringBuilder sql = new StringBuilder("SELECT * FROM " + obj.TableName);
            DBColumns parameters = new DBColumns();
            if (dic != null && dic.Count > 0)
            {
                StringBuilder sbValues = new StringBuilder();
                string clause = "";
                foreach (DBColumn keyVal in dic)
                {
                    if (sbValues.Length > 0)
                        clause = ((bool)useSearchAnd) ? " AND " : " OR ";

                    sbValues.Append(string.Format("{0} {1} = @_{1}"));
                    if (useLike)
                    {
                        parameters.Add(keyVal.Field, string.Format("%{0}%", keyVal.Value), DbType.String);
                    }
                    else
                    {
                        parameters.Add(keyVal);
                    }
                }

                sql.Append(" WHERE (" + sbValues.ToString() + ")");
            }

            if (!string.IsNullOrWhiteSpace(orderBy))
                sql.Append(" ORDER BY " + orderBy);

            DataTable dt = null;
            try
            {
                dt = _fillTable(sql.ToString(), parameters, "(get DataTable) select");
                return dt;
            }
            catch (Exception ex)
            {
                _writeLog(ex, sql.ToString());
                throw;
            }
        }
        #endregion

        #region executeTranSql
        /// <summary>
        /// Esegue le istruzioni sql contenute nell'array passato in un unica transazione.
        /// Il livello di isolamento utilizzato � quello di default.
        /// Registra eventuali eccezioni e le rilancia.
        /// </summary>
        /// <param name="arrSql">array di stringhe sql da eseguire</param>
        /// <exception cref="System.InvalidOperationException">System.InvalidOperationException</exception>
        /// <exception cref="System.Exception">System.Exception</exception>
        public static void executeTranSql(string[] arrSql)
        {
            _setLogSql("executeTranSql", string.Join(",", arrSql));

            IDbConnection cn = null;
            IDbTransaction tran = null;
            IDbCommand cmd = null;
            try { cn = ConnectionManager.Open(); }
            catch (Exception ex)
            {
                if (cn != null) cn.Close();
                _writeLog(ex, arrSql);
                throw;
            }
            try
            {
                //TODO: use parameters
                cmd = cn.CreateCommand();
                tran = cn.BeginTransaction();
                cmd.Transaction = tran;
                foreach (string sql in arrSql)
                {
                    cmd.CommandText = sql;
                    cmd.ExecuteNonQuery();
                }
                tran.Commit();
            }
            catch (Exception ex2)
            {
                if (tran != null) tran.Rollback();
                if (cn != null) cn.Close();
                _writeLog(ex2, arrSql);
                throw;
            }

        }
        #endregion

        #region getIDataReader
        /// <summary>
        /// Restituisce un IDataReader data una query valida.
        /// Non registra eventuali eccezioni, si limita a rilanciarle.
        /// </summary>
        /// <param name="sql">query sql</param>
        /// <returns>System.Data.IDataReader</returns>
        /// <exception cref="System.Exception">System.Exception</exception>
        public static IDataReader getIDataReader(string sql, DBColumns parameters)
        {
            // Sostituisco tutti gli @parametro con @_parametro per evitare keywords
            string replacedSql = Regex.Replace(sql, "@([^_]{1})", "@_$1");

            _setLogSql("getIDataReader", replacedSql);

            IDbConnection cn = null;
            try
            {
                cn = ConnectionManager.Open();
            }
            catch (Exception ex)
            {
                if (cn != null) cn.Close();
                _writeLog(ex, replacedSql);
                throw;
            }

            IDbCommand cmd = cn.CreateCommand();
            cmd.CommandText = replacedSql;
            cmd.CommandType = CommandType.Text;
            IDataReader reader = null;

            if (parameters != null)
            {
                parameters.AddToIDbCommand(cmd);
            }

            try
            {
                reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex2)
            {
                if (cn != null) cn.Close();
                _writeLog(ex2, replacedSql);
                throw;
            }

            return reader;
        }
        #endregion

        #region executeScalar
        /// <summary>
        /// Esegue l'istruzione passata. 
        /// In operazioni di insert, se getLastInsertedID � uguale a True restituisce l'ultimo id inserito (solo per database SQL SERVER e MySql).
        /// Non registra eventuali eccezioni, si limita a rilanciarle. 
        /// </summary>
        /// <param name="sql">string sql da eseguire</param>
        /// <param name="getLastInsertedID">In operazioni di insert ritorna l'ultimo ID inserito se il parametro � uguale a True.
        /// (solo per database SQL SERVER e MySql)</param>
        /// <returns>System.Object</returns>
        /// <exception cref="System.Exception">System.Exception</exception>
        public static object executeScalar(string sql, DBColumns parameters, bool getLastInsertedID)
        {
            // Sostituisco tutti gli @parametro con @_parametro per evitare keywords
            string replacedSql = Regex.Replace(sql, "@([^_]{1})", "@_$1");

            _setLogSql("executeScalar", replacedSql);

            IDbConnection cn = null;
            try { cn = ConnectionManager.Open(); }
            catch (Exception ex)
            {
                if (cn != null) cn.Close();
                _writeLog(ex, replacedSql);
                throw;
            }
            if (getLastInsertedID)
            {
                replacedSql += _getToken4LastInsertId();

            }

            IDbCommand cmd = cn.CreateCommand();
            cmd.CommandText = replacedSql;
            cmd.CommandType = CommandType.Text;

            if (parameters != null)
                parameters.AddToIDbCommand(cmd);

            object retObj = null;
            try
            {
                retObj = cmd.ExecuteScalar();
                cn.Close();
                return retObj;
            }
            catch (Exception ex2)
            {
                if (cn != null) cn.Close();
                _writeLog(ex2, replacedSql);
                throw;
            }
        }
        #endregion

        #region executeNonQuery
        /// <summary>
        /// Esegue l'istruzione sql passata e restituisce il numero di righe interessate.
        /// </summary>
        /// <param name="sql">istruzione sql</param>
        /// <returns>System.Int32</returns>
        /// <exception cref="System.InvalidOperationException">System.InvalidOperationException</exception>
        /// <exception cref="System.Exception">System.Exception</exception>
        public static int executeNonQuery(string sql, DBColumns parameters)
        {
            // Sostituisco tutti gli @parametro con @_parametro per evitare keywords
            string replacedSql = Regex.Replace(sql, "@([^_]{1})", "@_$1");

            _setLogSql("executeNonQuery", replacedSql);

            IDbConnection cn = null;
            try { cn = ConnectionManager.Open(); }
            catch (Exception ex)
            {
                if (cn != null) cn.Close();
                _writeLog(ex, replacedSql);
                throw;
            }
            IDbCommand cmd = cn.CreateCommand();
            cmd.CommandText = replacedSql;
            cmd.CommandType = CommandType.Text;

            if (parameters != null)
                parameters.AddToIDbCommand(cmd);

            int ret = 0;
            try
            {
                ret = cmd.ExecuteNonQuery();
                cn.Close();
                return ret;
            }
            catch (Exception ex2)
            {
                if (cn != null) cn.Close();
                _writeLog(ex2, replacedSql);
                throw;
            }
        }
        #endregion

        #endregion

    }
}
