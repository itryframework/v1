﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace it.itryframework.data
{
    public class DBColumns : List<DBColumn>
    {
        public static DBColumns FromDictionary(Dictionary<string, object> values)
        {
            DBColumns result = new DBColumns();
            foreach (KeyValuePair<string, object> pair in values)
                result.Add(new DBColumn(pair.Key, pair.Value));
            return result;
        }

        public void AddAll(List<DBColumn> columns)
        {
            foreach (DBColumn column in columns)
                Add(column);
        }

        public new void Add(DBColumn column)
        {
            if (column == null)
                return;

            foreach (DBColumn existing in this)
            {
                if (existing.Field == column.Field)
                {
                    existing.Value = column.Value;
                    existing.Type = column.Type;
                    return;
                }
            }

            base.Add(column);
        }

        public void Add(string field, object value)
        {
            Add(field, value, GetDbTypeFromValue(value));
        }

        public void Add(string field, object value, DbType type)
        {
            object _value = value;
            if (value == null)
            {
                _value = DBNull.Value;
            }
            else if (type == DbType.DateTime || type == DbType.Date)
            {
                // Avoid this errors
                // http://stackoverflow.com/questions/15157328/datetime-minvalue-and-sqldatetime-overflow
                // https://ammarfassy.wordpress.com/2011/05/11/passing-null-values-to-sql-server-sqldatetime-and-datetime/
                if (value == null)
                {
                    _value = DBNull.Value;
                }
                else
                {
                    DateTime dtValue = (DateTime)value;
                    if (dtValue == DateTime.MinValue)
                        _value = DBNull.Value;
                }
            }

            Add(new DBColumn(field, _value, type));
        }

        public static DBColumns FromDictionary(IDictionary<string, object> values)
        {
            DBColumns result = new DBColumns();
            if (values == null || values.Count <= 0)
                return result;

            foreach (KeyValuePair<string, object> pair in values)
                result.Add(pair.Key, pair.Value);

            return result;
        }

        public static DBColumns Single(string field, object value, DbType type)
        {
            return new DBColumns() { new DBColumn(field, value, type) };
        }

        public static DBColumns Single(string field, object value)
        {
            return new DBColumns() { new DBColumn(field, value, GetDbTypeFromValue(value)) };
        }

        public static DbType GetDbTypeFromValue(object value)
        {
            // TODO: add missing types

            if (value is int)
                return DbType.Int32;

            if (value is long)
                return DbType.Int64;

            if (value is short)
                return DbType.Int16;

            if (value is decimal)
                return DbType.Decimal;

            if (value is bool)
                return DbType.Boolean;

            if (value is double || value is float)
                return DbType.Double;

            if (value is DateTime)
                return DbType.DateTime;

            return DbType.String;
        }

        public static SqlDbType GetSqlDbTypeFromValue(object value)
        {
            // TODO: add missing types

            if (value is int)
                return SqlDbType.Int;

            if (value is long)
                return SqlDbType.BigInt;

            if (value is short)
                return SqlDbType.SmallInt;

            if (value is decimal)
                return SqlDbType.Decimal;

            if (value is bool)
                return SqlDbType.Bit;

            if (value is double || value is float)
                return SqlDbType.Float;

            if (value is DateTime)
                return SqlDbType.DateTime;

            return SqlDbType.NVarChar;
        }

        public void AddToIDbCommand(IDbCommand cmd)
        {
            foreach (DBColumn column in this)
            {
                IDataParameter parameter = cmd.CreateParameter();
                parameter.ParameterName = "@_" + column.Field;
                parameter.Value = column.Value;
                parameter.DbType = column.Type;
                cmd.Parameters.Add(parameter);
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (DBColumn item in this)
                sb.AppendLine(item.ToString());
            return sb.ToString();
        }
    }
}
