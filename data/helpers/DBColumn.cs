﻿using System.Data;

namespace it.itryframework.data
{
    public class DBColumn
    {
        public DBColumn(string field, object value, DbType type)
        {
            Field = field;
            Value = value;
            Type = type;
        }

        public DBColumn(string field, object value) : this(field, value, DbType.String)
        {

        }

        public string Field { get; set; }

        public object Value { get; set; }

        public DbType Type { get; set; }

        public static bool IsValid (DBColumn column)
        {
            if (column == null)
                return false;

            if (string.IsNullOrEmpty(column.Field))
                return false;

            return true;
        }

        public static DBColumns Single(string field, object value, DbType type)
        {
            return DBColumns.Single(field, value, type);
        }

        public static DBColumns Single(string field, object value)
        {
            return DBColumns.Single(field, value);
        }

        public static bool IsIntegerType (DbType dataType)
        {
            if (dataType == DbType.Int16 || dataType == DbType.Int32 || dataType == DbType.Int64)
                return true;

            if (dataType == DbType.UInt16 || dataType == DbType.UInt32 || dataType == DbType.UInt64)
                return true;

            return false;
        }

        public static bool IsStringType(DbType dataType)
        {
            if (dataType == DbType.AnsiString || dataType == DbType.AnsiStringFixedLength || dataType == DbType.String || dataType == DbType.StringFixedLength)
                return true;

            return false;
        }

        public override string ToString()
        {
            return string.Format("Field: {0}; Value: {1}; Type: {2}", Field, Value, Type);
        }
    }
}
