using System;
using System.Collections.Generic;
using System.Text;

namespace it.itryframework.interfaces
{
    internal interface ILog
    {
        bool manageError(it.itryframework.config.ErrorConfigurationSection errorSection,Exception ex, string customMessage);

    }
}
