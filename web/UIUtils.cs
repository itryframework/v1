using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace it.itryframework.web
{
    public class UIUtils
    {
        /// <summary>
        /// Data una label crea il percorso in base ai parametri passati. Se la label passata contiene gi� altri controlli il 
        /// seguente viene aggiunto alla fine.
        /// </summary>
        /// <param name="lbl">Label alla quale aggiungere i controlli che verranno creati</param>
        /// <param name="text">testo (per gli hyperlink)</param>
        /// <param name="pathTo">pagina destinataria</param>
        /// <param name="addLiteral">aggiunge -></param>
        public void setLabelPercoso(Label lbl, Dictionary<string, string> dic)
        {
            lbl.Controls.Clear();
            foreach (KeyValuePair<string, string> keyVal in dic)
            {
                int pos = lbl.Controls.Count;
                LiteralControl lit = new LiteralControl(" -> ");
                HyperLink hyp = new HyperLink();
                hyp.Text = keyVal.Key;
                hyp.NavigateUrl = keyVal.Value;
                hyp.ForeColor = System.Drawing.Color.Blue;
                hyp.CssClass = "titolo2";
                lbl.Controls.AddAt(pos, hyp);
                lbl.Controls.AddAt(pos + 1, lit);
            }
            lbl.Controls.RemoveAt(lbl.Controls.Count - 1);  //tolgo l'ultimo -> inserito
        }
        /// <summary>
        /// Riempie l'istanza della DropDownList passata con i valori contenuti id DataTable.
        /// </summary>
        /// <param name="dt">DataTable contenente i valori da estrarre</param>
        /// <param name="ddl">istanza DropDownList</param>
        /// <param name="text">propriet� text della DropDownList</param>
        /// <param name="value">propriet� value della DropDownList</param>
        public void fillDDL(DataTable dt, DropDownList ddl, string text, string value)
        {
            fillDDL(dt, ddl, text, value, null, null, -1);
        }
        /// <summary>
        /// Riempie l'istanza della DropDownList passata con i valori contenuti id DataTable.
        /// Se valueSelected � un valore valido lo utilizza per impostare un valore selected.
        /// </summary>
        /// <param name="dt">DataTable contenente i valori da estrarre</param>
        /// <param name="ddl">istanza DropDownList</param>
        /// <param name="text">propriet� text della DropDownList</param>
        /// <param name="value">propriet� value della DropDownList</param>
        /// <param name="valueSelected">valore da rendere selected</param>
        public void fillDDL(DataTable dt, DropDownList ddl, string text, string value, string valueSelected)
        {
            fillDDL(dt, ddl, text, value, valueSelected, null, -1);
        }
        /// <summary>
        /// Riempie l'istanza della DropDownList passata con i valori contenuti id DataTable.
        /// Se valueSelected � un valore valido lo utilizza per impostare un valore selected.
        /// Inserisce customItem alla posizione indicata da posCustomItem.
        /// </summary>
        /// <param name="dt">DataTable contenente i valori da estrarre</param>
        /// <param name="ddl">istanza DropDownList</param>
        /// <param name="text">propriet� text della DropDownList</param>
        /// <param name="value">propriet� value della DropDownList</param>
        /// <param name="valueSelected">valore da rendere selected</param>
        /// <param name="customItem">customItem da inserire</param>
        /// <param name="posCustomItem">posizione dove inserire il customItem</param>
        public void fillDDL(DataTable dt, DropDownList ddl, string text, string value, string valueSelected, ListItem customItem, int posCustomItem)
        {
            if (dt == null || dt.Rows.Count == 0) return;
            ListItem selItem = null;
            if (string.IsNullOrEmpty(valueSelected))
            {
                selItem = ddl.SelectedItem;
            }
            else
            {
                selItem = new ListItem();
                selItem.Value = valueSelected;
            }

            ddl.Items.Clear();

            foreach (DataRow row in dt.Rows)
            {
                ListItem item = new ListItem(Convert.ToString(row[text]), Convert.ToString(row[value]));
                ddl.Items.Add(item);
                if (selItem != null && ddl.Items.FindByValue(selItem.Value) != null)
                    ddl.Items.FindByValue(selItem.Value).Selected = true;
            }
            if (customItem != null && posCustomItem >= 0)
            {
                //aggiungo il custom item
                ddl.Items.Insert(posCustomItem, customItem);
            }
        }
    }
}
