using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace it.itryframework.config
{
    public sealed class DALConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("tipodb", IsRequired = true)]
        public string TipoDb
        {
            get { return this["tipodb"] as string; }
        }
        [ConfigurationProperty("connstring", IsRequired = true)]
        public string ConnString
        {
            get { return this["connstring"] as string; }
        }
        [ConfigurationProperty("managerCryptoName", IsRequired = false)]
        public string ManagerCryptoName
        {
            get { return this["managerCryptoName"] as string; }
        }
        [ConfigurationProperty("decryptpwd", IsRequired = false, DefaultValue = false)]
        public bool DecryptPwd
        {
            get
            {
                try
                {
                    bool _decrypt;
                    bool.TryParse(this["decryptpwd"].ToString(), out _decrypt);
                    return _decrypt;
                }
                catch { return false; }
            }
        }
    }
}
