using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace it.itryframework.config
{
    public sealed class MailConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("smtp", IsRequired = true)]
        public string Smtp
        {
            get { return this["smtp"] as string; }
        }

        [ConfigurationProperty("authenticationUser", IsRequired = false)]
        public string AuthenticationUser
        {
            get { return this["authenticationUser"] as string; }
        }

        [ConfigurationProperty("authenticationPwd", IsRequired = false)]
        public string AuthenticationPwd
        {
            get { return this["authenticationPwd"] as string; }
        }

        [ConfigurationProperty("mailFrom", IsRequired = false)]
        public string MailFrom
        {
            get { return this["mailFrom"] as string; }
        }

        [ConfigurationProperty("mailFromName", IsRequired = false)]
        public string MailFromName
        {
            get { return this["mailFromName"] as string; }
        }

        [ConfigurationProperty("mailBccs", IsRequired = false)]
        public string MailBCCs
        {
            get { return this["mailBccs"] as string; }
        }

        [ConfigurationProperty("smtpPort", IsRequired = false)]
        public string SmtpPort
        {
            get { return this["smtpPort"] as string; }
        }

        [ConfigurationProperty("smtpSsl", IsRequired = false)]
        public string SmtpSSL
        {
            get { return this["smtpSsl"] as string; }
        }
    }
}
