using System;
using System.Configuration;

namespace it.itryframework.config
{
    public sealed class LogConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("sqlToVsOutputWindow", IsRequired = false)]
        public bool SqlToVsOutputWindow
        {
            get { return Convert.ToBoolean(this["sqlToVsOutputWindow"]); }
        }
    }
}
