using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace it.itryframework.config
{
    public sealed class ErrorConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("managerClassName", IsRequired = true)]
        public string ManagerClassName
        {
            get { return this["managerClassName"] as string; }
        }

        [ConfigurationProperty("smtp", IsRequired = false)]
        public string Smtp
        {
            get { return this["smtp"] as string; }
        }

        [ConfigurationProperty("authenticationUser", IsRequired = false)]
        public string AuthenticationUser
        {
            get { return this["authenticationUser"] as string; }
        }

        [ConfigurationProperty("authenticationPwd", IsRequired = false)]
        public string AuthenticationPwd
        {
            get { return this["authenticationPwd"] as string; }
        }

        [ConfigurationProperty("mailFrom", IsRequired = false)]
        public string MailFrom
        {
            get { return this["mailFrom"] as string; }
        }

        [ConfigurationProperty("mailFromName", IsRequired = false)]
        public string MailFromName
        {
            get { return this["mailFromName"] as string; }
        }

        [ConfigurationProperty("mailTo", IsRequired = false)]
        public string MailTo
        {
            get { return this["mailTo"] as string; }
        }

        [ConfigurationProperty("mailBccs", IsRequired = false)]
        public string MailBCCs
        {
            get { return this["mailBccs"] as string; }
        }

        [ConfigurationProperty("smtpPort", IsRequired = false)]
        public string SmtpPort
        {
            get { return this["smtpPort"] as string; }
        }

        [ConfigurationProperty("smtpSsl", IsRequired = false)]
        public string SmtpSSL
        {
            get { return this["smtpSsl"] as string; }
        }

        [ConfigurationProperty("mailSubjectPrefix", IsRequired = false)]
        public string MailSubjectPrefix
        {
            get { return this["mailSubjectPrefix"] as string; }
        }

        [ConfigurationProperty("enable", IsRequired = false, DefaultValue = true)]
        public bool Enable
        {
            get
            {
                try
                {
                    bool _enable;
                    bool.TryParse(this["enable"].ToString(), out _enable);
                    return _enable;
                }
                catch { return false; }
            }
        }

        [ConfigurationProperty("mailBcc", IsRequired = false)]
        public string MailBcc
        {
            get { return this["mailBcc"] as string; }
        }
    }
}
