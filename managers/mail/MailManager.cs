using System;
using it.itryframework.config;
using MailKit.Net.Smtp;
using MimeKit;

namespace it.itryframework.managers.mail
{
    public class MailManager
    {
        private MailConfigurationSection mailSection;
        
        public MailManager()
        {
            mailSection = it.itryframework.managers.config.ConfigManager.getMailConfigValues();
        }
        
        public void send(string from, string to, string subject, string body)
        {
            _send(from, null, to, null, null, null, subject, body);
        }

        public void send(string from, string to, string[] bcc, string subject, string body)
        {
            _send(from, null, to, null, bcc, null, subject, body);
        }

        public void send(string from, string fromName, string to, string toName, string subject, string body)
        {
            _send(from, fromName, to, toName, null, null, subject, body);
        }    

        public void send(string from, string nameFrom, string to, string toName, string[] bcc, string emailReplyTo, string subject, string body)
        {
            _send(from, nameFrom, to, toName, bcc, emailReplyTo, subject, body);
        }

        private void _send(string from, string nameFrom, string to, string toName, string[] bcc, string emailReplyTo, string subject, string body)
        {

            int port = Convert.ToInt32(mailSection.SmtpPort);
            string smtp = mailSection.Smtp;

            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(from, from));
            message.To.Add(new MailboxAddress(to, to));

            if (bcc != null && bcc.Length > 0)
            {
                for (int i = 0; i < bcc.Length; i++)
                {
                    message.Bcc.Add(new MailboxAddress(bcc[i], bcc[i]));
                }
            }

            message.Subject = subject;

            var bodyBuilder = new BodyBuilder();
            bodyBuilder.HtmlBody = body;

            message.Body = bodyBuilder.ToMessageBody();


            using (var client = new SmtpClient())
            {
                client.Connect(smtp, port, false);


                if (mailSection.SmtpSSL == "1")
                {
                    client.Authenticate(mailSection.AuthenticationUser, mailSection.AuthenticationPwd);
                }

                client.Send(message);
                client.Disconnect(true);
            }
        }
    }

}