using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Collections.Generic;
using System.Text;
using it.itryframework.exception;
using it.itryframework.config;
using it.itryframework.costanti;

namespace it.itryframework.managers.connection
{
    internal class ConnectionManager
    {
        private static DALConfigurationSection m_dalSection = null;

        /// <summary>
        /// Ottiene il tipo di database a cui � associata la connessione.
        /// </summary>
        public static string getDbType()
        {
            return m_dalSection.TipoDb;
        }
	
        private ConnectionManager() { }

        private static void load()
        {
            try
            {
                m_dalSection = it.itryframework.managers.config.ConfigManager.getDALConfigValues();
            }
            catch (ITryFrameworkException itex)
            {
                throw itex;
            }
        }

        /// <summary>
        /// Legge i parametri dal web.config ed una apre una connessine al database indicato.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.Exception">System.Exception</exception>
        public static IDbConnection Open()
        {
            load();
            IDbConnection cn = null;
            if (m_dalSection.TipoDb.Equals(cosNomiTipiDb.SQLServer)) cn = new SqlConnection();
            else if (m_dalSection.TipoDb.Equals(cosNomiTipiDb.MSAccess)) cn = new OleDbConnection();
            else cn = new System.Data.Odbc.OdbcConnection();
            cn.ConnectionString = m_dalSection.ConnString;

            if (m_dalSection.DecryptPwd) {
                it.itryframework.managers.encryption.CryptoManager cryptoMng=null;
                if (string.IsNullOrEmpty(m_dalSection.ManagerCryptoName))
                {
                    //usa il default
                     cryptoMng= new it.itryframework.managers.encryption.CryptoManager();
                }
                else
                {
                    //usa quello definito
                    //System.Runtime.Remoting.ObjectHandle objHandle = Activator.CreateInstance(AppDomain.CurrentDomain,"testweb.manager", "MyCriptoManager");
                    //System.Runtime.Remoting.ObjectHandle objHandle = AppDomain.CurrentDomain.CreateInstance("MyCriptoManager", m_dalSection.ManagerCryptoName);   
                    //cryptoMng = (it.itryframework.managers.encryption.CryptoManager)objHandle.Unwrap();



                }
                
                cn.ConnectionString = cryptoMng.getDecrypted();
            }


            try
            {
                cn.Open();
                return cn;
            }
            catch (Exception ex)
            {
                if (cn != null)cn.Close();
                cn = null;
                throw;
            }
            
        }
    }
}
