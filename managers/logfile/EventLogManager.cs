using System;
using System.Collections.Generic;
using System.Text;
using it.itryframework.exception;

namespace it.itryframework.managers.log
{
    /// <summary>
    /// Classe per la gestione errori nel registro eventi di Windows
    /// </summary>
    public sealed class EventLogManager : it.itryframework.interfaces.ILog
    {
        public EventLogManager() { }
        
        #region ILog Membri di

        public bool manageError(it.itryframework.config.ErrorConfigurationSection errorSection, Exception ex, string customMessage)
        {
            System.Diagnostics.EventLog.WriteEntry(customMessage, "SOURCE: " + ex.Source + " MESSAGE: " + ex.Message + " STACKTRACE: " + ex.StackTrace);
            return true;
        }

        #endregion
    }
}
