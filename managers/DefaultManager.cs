using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Data;
using it.itryframework;
using it.itryframework.data;

namespace it.itryframework.managers
{
    public class DefaultManager<T> where T : it.itryframework.interfaces.IGenericObject
    {
        public DefaultManager() { }

        /// <summary>
        /// Riempie l'oggetto con i dati prelevati dal db.
        /// </summary>
        /// <param name="tClass">oggetto</param>
        /// <returns></returns>
        public virtual bool select(T tClass) { return DBMapper.select(tClass); }
        /// <summary>
        /// Riempie l'oggetto con i dati prelevati dal db.
        /// Le eccezioni dovranno essere gestite dal chiamante. 
        /// </summary>
        /// <param name="tClass">oggetto</param>
        /// <param name="pKeyFieldName">nome della colonna che contiene la chiave primaria</param>
        /// <param name="id">valore della chiave primaria</param>
        /// <returns></returns>
        public virtual bool select(T tClass, string pKeyFieldName, string id)
        {
            return DBMapper.select(tClass, pKeyFieldName, id);
        }

        /// <summary>
        /// Riempie l'oggetto con i dati prelevati dal db.
        /// Le eccezioni dovranno essere gestite dal chiamante. 
        /// </summary>
        /// <param name="tClass">oggetto</param>
        /// <param name="pKeyFieldName">nome della colonna che contiene la chiave primaria</param>
        /// <param name="id">valore della chiave primaria</param>
        /// <returns></returns>
        public virtual bool select(T tClass, string pKeyFieldName, int id)
        {
            return DBMapper.select(tClass, pKeyFieldName, id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public virtual T[] select(string sql)
        {
            return DBMapper.select<T>(sql);
        }

        /// <summary>
        /// Cancella dal db l'oggetto passato.
        /// Le eccezioni dovranno essere gestite dal chiamante. 
        /// </summary>
        /// <param name="tClass">oggetto da cancellare</param>
        /// <returns></returns>
        public virtual bool delete(T tClass)
        {
            return DBMapper.delete(tClass);
        }

        /// <summary>
        /// Aggiorna il db con i dati contenuti in oggetto passato.
        /// Le eccezioni dovranno essere gestite dal chiamante.
        /// </summary>
        /// <param name="tClass">oggetto</param>
        /// <returns></returns>
        public virtual bool update(T tClass)
        {
            return DBMapper.update(tClass);
        }

        /// <summary>
        /// Aggiorna il db con i dati contenuti in oggetto passato e con le clausole where contenute in dictionary
        /// (chiave ==> nome della colonna, valore ==> (tipo object)valore di tipo int o string.
        /// Le eccezioni dovranno essere gestite dal chiamante.
        /// </summary>
        /// <param name="tClass">oggetto</param>
        /// <param name="dicWhere">dictionary contenente nomi colonne e valori per le clausole where</param>
        /// <returns></returns>
        public virtual bool update(T tClass, Dictionary<string, object> dicWhere)
        {
            return DBMapper.update(tClass, dicWhere);
        }

        /// <summary>
        /// Inserisce in db l'oggetto passato e restituisce l'id della riga inserita.
        /// (per database Access non ritorna l'ultimo id inserito)
        /// Le eccezioni dovranno essere gestite dal chiamante. 
        /// </summary>
        /// <param name="tClass"></param>
        /// <returns></returns>
        public virtual int insert(T tClass)
        {
            return DBMapper.insert(tClass);
        }
    }
}


